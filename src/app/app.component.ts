import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public employeeData: Array<string> = [];
  public data: Array<string> = [];
  p: 1;
  constructor(private service: DataService) {
  }

  ngOnInit() {
    this.getEmployees();
  }

  getEmployees() {
    this.service.getEmployees().subscribe(data => {
      this.employeeData = data.data;
    });
  }
}
