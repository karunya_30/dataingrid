import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ChildComponent } from './child/child.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { DataService } from './data.service';


@NgModule({
  declarations: [
    AppComponent,
    ChildComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
